// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftPackage",
    products: [
        // Products define the executables and libraries produced by a package, and make them visible to other packages.
        .library(
            name: "SwiftPackage",
            targets: ["SwiftPackage"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/ReactiveX/RxSwift.git", from: "5.0.0"),
        .package(url: "https://github.com/Swinject/Swinject", from: "2.6.0"),
        //.package(url: "https://github.com/Swinject/SwinjectStoryboard", from: "2.2.0"),
        //.package(url: "https://github.com/rhodgkins/SwiftHTTPStatusCodes", from: "3.3.0")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages which this package depends on.
        .target(
            name: "SwiftPackage",
            dependencies: [
                "RxSwift", "RxCocoa", "RxRelay",
                "Swinject",
                //"SwinjectStoryboard",
                //"HTTPStatusCodes"
            ]
        ),
        .testTarget(
            name: "SwiftPackageTests",
            dependencies: ["SwiftPackage"]),
    ]
)
