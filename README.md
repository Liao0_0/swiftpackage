# SwiftPackage

## Commands after Adding a Package
1. `swift package update`
2. `swift package generate-xcodeproj`

## Current Supported Packages

1. .package(url: "https://github.com/ReactiveX/RxSwift.git", from: "5.0.0")
with "RxSwift", "RxCocoa", "RxRelay"
2. .package(url: "https://github.com/Swinject/Swinject", from: "2.6.0")
with "Swinject"
